<?php
/**
 * @file
 * Basic functions and hooks for the addressfield_ro module.
 */

define('ADDRESSFIELD_RO_BASE_TABLE', 'addressfield_ro');

/**
 * Implements hook_ctools_plugin_directory().
 */
function addressfield_ro_ctools_plugin_directory($module, $plugin) {
  if ($module == 'addressfield') {
    return 'plugins/' . $plugin;
  }
}

/**
 * Implements hook_menu().
 */
function addressfield_ro_menu() {
  $items = array();
  $basepath = addressfield_ro_basepath();
  // Romanian addresses list.
  $items[$basepath] = array(
    'title' => 'Romanian Addresses',
    'description' => 'Manage Romanian addresses that can be used in address fields.',
    'page callback' => 'addressfield_ro_admin',
    'access callback' => 'addressfield_ro_access',
    'access arguments' => array(array('create', 'edit', 'delete')),
    'file' => 'includes/addressfield_ro.pages.inc',
  );
  $items["{$basepath}/default"] = array(
    'title' => 'Address list',
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );
  // Csv importer. Do we need this?
  $items["{$basepath}/import"] = array(
    'title' => 'Import CSV files',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('addressfield_ro_import_form'),
    'type' => MENU_LOCAL_TASK,
    'access callback' => 'addressfield_ro_access',
    'access arguments' => array(array('import')),
    'file' => 'includes/addressfield_ro.import.inc',
  );
  // Edit Romanian addresses.
  $items["$basepath/%/edit"] = array(
    'title' => 'Edit',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('addressfield_ro_edit_form', 3),
    'access callback' => 'addressfield_ro_access',
    'access arguments' => array(array('edit')),
    'file' => 'includes/addressfield_ro.pages.inc',
  );
  // Delete Romanian addresses.
  $items["{$basepath}/%/delete"] = array(
    'title' => 'Delete',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('addressfield_ro_delete_form', 3),
    'access callback' => 'addressfield_ro_access',
    'access arguments' => array(array('delete')),
    'file' => 'includes/addressfield_ro.pages.inc',
  );
  // Autocomplete for Romanian addresses.
  $items['addressfield_ro/autocomplete/%'] = array(
    'title' => 'Addressfieldro autocomplete',
    'page callback' => 'addressfield_ro_autocomplete',
    'page arguments' => array(2),
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
    'file' => 'includes/addressfield_ro.pages.inc',
  );

  return $items;
}

/**
 * Getter for module basepath.
 *
 * @return string
 *   The basepath.
 */
function addressfield_ro_basepath() {
  return 'admin/structure/addressfield-ro';
}

/**
 * Implements hook_permission().
 */
function addressfield_ro_permission() {
  return array(
    'administer romanian address' => array(
      'title' => t('Administer romanian address'),
      'description' => t('Add, edit and delete any address from romanian address database.'),
    ),
    'create romanian address' => array(
      'title' => t('Create romanian address'),
    ),
    'edit romanian address' => array(
      'title' => t('Edit any romanian address'),
    ),
    'delete romanian address' => array(
      'title' => t('Delete any romanian address'),
    ),
    'import romanian address' => array(
      'title' => t('Import romanian addresses'),
    ),
  );
}

/**
 * Access callback for addressfield_ro.
 *
 * @param array $permissions
 *   A list of permission strings to check access for.
 *
 * @return bool
 *   True if the user has at least one permission, false otherwise.
 *
 * @see addressfield_ro_permission()
 */
function addressfield_ro_access(array $permissions) {
  if (user_access('administer romanian address')) {
    return TRUE;
  }
  foreach ($permissions as $perm) {
    if (user_access($perm . ' romanian address')) {
      return TRUE;
    }
  }
}

/**
 * Implements hook_addressfield_address_formats_alter().
 */
function addressfield_ro_addressfield_address_formats_alter(&$address_formats) {
  // Add administrative area to render elements.
  $address_formats['RO']['used_fields'][] = 'administrative_area';
  $address_formats['RO']['render_administrative_area_value'] = TRUE;
}

/**
 * Save address to database.
 *
 * @param array $address
 *   The address array.
 *
 * @return bool
 *   The status of saving operation.
 */
function addressfield_ro_address_save(array $address) {
  $cache = &drupal_static('addressfield_ro');

  $fields = array(
    'administrative_area' => isset($address['administrative_area']) ? $address['administrative_area'] : '',
    'locality' => isset($address['locality']) ? $address['locality'] : '',
  );

  // Create.
  if (!isset($address['id']) || empty($address['id'])) {
    $insert = db_insert(ADDRESSFIELD_RO_BASE_TABLE)
      ->fields($fields)
      ->execute();
    if ($insert) {
      // Update the cache.
      $cache[$insert] = $fields;
      // Prepare the return array.
      $return = $fields;
      $return['id'] = $insert;
      return $return;
    }

    return FALSE;
  }
  // Edit.
  if (is_numeric($address['id'])) {
    $fields = array(
      'administrative_area' => isset($address['administrative_area']) ? $address['administrative_area'] : '',
      'locality' => isset($address['locality']) ? $address['locality'] : '',
    );

    $exists = addressfield_ro_address_load($address['id']);
    if ($exists) {
      db_update(ADDRESSFIELD_RO_BASE_TABLE)
          ->fields($fields)
          ->condition('id', $address['id'], '=')
          ->execute();
      // Update the cache.
      $cache[$address['id']] = $fields;
      // Prepare the return array.
      $return = $fields;
      $return['id'] = $address['id'];
      return $return;
    }

    return FALSE;
  }

  return FALSE;
}

/**
 * Delete address from database.
 *
 * @param mixed $address
 *   The address array or the address id.
 *
 * @return bool
 *   The status of saving operation.
 */
function addressfield_ro_address_delete($address) {
  if (isset($address['id']) && is_numeric($address['id'])) {
    $id = $address['id'];
  }
  elseif (is_numeric($address)) {
    $id = $address;
  }
  else {
    return FALSE;
  }

  return db_delete(ADDRESSFIELD_RO_BASE_TABLE)
    ->condition('id', $id, '=')
    ->execute();
}

/**
 * Load address from database.
 */
function addressfield_ro_address_load($id = NULL) {
  if (is_null($id)) {
    return FALSE;
  }
  $address = &drupal_static('addressfield_ro', array());
  if (array_key_exists($id, $address)) {
    return $address[$id];
  }
  $query = "SELECT * FROM {" . ADDRESSFIELD_RO_BASE_TABLE . "} WHERE id = :id";
  $result = db_query($query, array(':id' => $id));
  if ($result = $result->fetchAssoc()) {
    $address[$result['id']] = $result;
  }
  else {
    $address[$id] = FALSE;
  }
  return $address[$id];
}

/**
 * Helper function to get counties.
 */
function addressfield_ro_get_counties() {
  return array(
    '' => '- ' . t('Select') . ' -',
    'AB' => 'Alba',
    'AR' => 'Arad',
    'AG' => 'Argeș',
    'B' => 'București',
    'BC' => 'Bacău',
    'BH' => 'Bihor',
    'BN' => 'Bistrița-Năsăud',
    'BT' => 'Botoșani',
    'BR' => 'Brăila',
    'BV' => 'Brașov',
    'BZ' => 'Buzău',
    'CL' => 'Călărași',
    'CS' => 'Caraș-Severin',
    'CJ' => 'Cluj',
    'CT' => 'Constanța',
    'CV' => 'Covasna',
    'DB' => 'Dâmbovița',
    'DJ' => 'Dolj',
    'GL' => 'Galați',
    'GR' => 'Giurgiu',
    'GJ' => 'Gorj',
    'HR' => 'Harghita',
    'HD' => 'Hunedoara',
    'IL' => 'Ialomița',
    'IS' => 'Iași',
    'IF' => 'Ilfov',
    'MM' => 'Maramureș',
    'MH' => 'Mehedinți',
    'MS' => 'Mureș',
    'NT' => 'Neamț',
    'OT' => 'Olt',
    'PH' => 'Prahova',
    'SJ' => 'Sălaj',
    'SM' => 'Satu Mare',
    'SB' => 'Sibiu',
    'SV' => 'Suceava',
    'TR' => 'Teleorman',
    'TM' => 'Timiș',
    'TL' => 'Tulcea',
    'VL' => 'Vâlcea',
    'VS' => 'Vaslui',
    'VN' => 'Vrancea',
  );
}
