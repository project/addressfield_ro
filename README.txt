Addressfield Romania Module
------------------------------

Description
-----------
Provides an add-on for the addressfield module for Romanian addresses.

Installation 
------------
 + Copy the module's directory to your modules directory.
 + Activate the module.
